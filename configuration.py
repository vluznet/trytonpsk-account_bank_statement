# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, ModelSingleton, fields


__all__ = ['BankStatementConfiguration']


class BankStatementConfiguration(ModelSingleton, ModelSQL, ModelView):
    'Bank Statement Configuration'
    __name__ = 'account.bank_statement.configuration'
    default_journal_bank = fields.Many2One('account.journal',
            'Default Journal Banks')

    @classmethod
    def __setup__(cls):
        super(BankStatementConfiguration, cls).__setup__()
