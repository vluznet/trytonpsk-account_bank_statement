#The COPYRIGHT file at the top level of this repository contains the full
#copyright notices and license terms.

from trytond.pool import Pool
from .statement import (BankStatement, BankStatementLine, BankReconciliation,
    BankStatementTransaction, UpdateBankStatementLines,
    SearchBankUnreconciledsLinesStart, CreateBankStatement,
    SearchBankUnreconciledLines, CreateBankStatementStart)
from .account import Account
from .move import (Move, Line)
from .bank import (Bank, BankAccount)
from .configuration import BankStatementConfiguration

def register():
    Pool.register(
        BankStatementConfiguration,
        Account,
        Bank,
        BankAccount,
        BankStatement,
        BankStatementLine,
        BankStatementTransaction,
        Move,
        Line,
        SearchBankUnreconciledsLinesStart,
        CreateBankStatementStart,
        BankReconciliation,
        module='account_bank_statement', type_='model')
    Pool.register(
        UpdateBankStatementLines,
        SearchBankUnreconciledLines,
        CreateBankStatement,
        module='account_bank_statement', type_='wizard')
