#The COPYRIGHT file at the top level of this repository contains the full
#copyright notices and license terms.

from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from decimal import Decimal

__all__ = ['Move', 'Line']

_ZERO = Decimal('0.00')


class Move(metaclass=PoolMeta):
    __name__ = 'account.move'

    @classmethod
    def post(cls, moves):
        res = super(Move, cls).post(moves)
        BankReconciliation = Pool().get('bank_statement.line-account.move.line')
        # brs - Bank Reconciliations
        create_brs = []
        for move in moves:
            for line in move.lines:
                if line.account.bank_reconcile:
                    pass
        if create_brs:
            BankReconciliation.create(create_brs)
        return res

    """
    @classmethod
    def draft(cls, moves):
        res = super(Move, cls).draft(moves)
        BankMove = Pool().get('bank_statement.line-account.move.line')
        delete_bank_lines = []
        for move in moves:
            for line in move.lines:
                delete_bank_lines += [x for x in line.bank_lines]
        BankMove.delete(delete_bank_lines)
        return res
    """

class Line(metaclass=PoolMeta):
    __name__ = 'account.move.line'
    bank_lines = fields.One2Many('bank_statement.line-account.move.line',
        'move_line', 'Conciliation Lines', readonly=True)
    bank_reconciled = fields.Boolean('Bank Reconciled', states={
            'readonly': True,
        })

    @classmethod
    def __setup__(cls):
        super(Line, cls).__setup__()
        cls._error_messages.update({
                'line_reconciled': ('Line "%(line)s" already reconciled with '
                'bank statement line "%(statement_line)s" with amont '
                '"%(amount)s". Please remove bank conciliation '
                'and try again.'),
                })

    @classmethod
    def _get_origin(cls):
        return (super(Line, cls)._get_origin()
            + ['account.bank_statement.line'])

    @classmethod
    def delete(cls, lines):
        super(Line, cls).delete(lines)
        for line in lines:
            for bank_line in line.bank_lines:
                if bank_line.bank_statement_line:
                    bank_line.raise_user_error('line_reconciled', {
                        'line': line.rec_name,
                        'statement_line':
                            bank_line.bank_statement_line.rec_name,
                        'amount': bank_line.amount,
                        })

    @classmethod
    def copy(cls, lines, default=None):
        if default is None:
            default = {}
        default['bank_lines'] = None
        super(Line, cls).copy(lines, default)

    @classmethod
    def get_bank_amounts(cls, lines, names):
        res = {}
        all_names = ('bank_amount', 'unreconciled_amount', 'bank_reconciled')
        for name in all_names:
            res[name] = {}.fromkeys([x.id for x in lines], 0)

        for line in lines:
            for bank_line in line.bank_lines:
                if 'bank_amount' in names and bank_line.statement_line:
                    res['bank_amount'][line.id] += bank_line.amount
                if 'unreconciled_amount' in names and \
                        not bank_line.statement_line:
                    res['unreconciled_amount'][line.id] += bank_line.amount
            if 'bank_reconciled' in names:
                res['bank_reconciled'][line.id] = False
                if res['unreconciled_amount'][line.id] == _ZERO:
                    res['bank_reconciled'][line.id] = True
        for name in all_names:
            if not name in names:
                del res[name]
        return res
