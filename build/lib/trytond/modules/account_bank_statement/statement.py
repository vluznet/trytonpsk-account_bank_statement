#The COPYRIGHT file at the top level of this repository contains the full
#copyright notices and license terms.

from decimal import Decimal
import datetime
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.pyson import Eval, If, Not, Equal
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.wizard import Wizard, StateTransition, StateView, Button


__all__ = ['BankStatement', 'BankStatementLine', 'BankReconciliation',
    'BankStatementTransaction', 'UpdateBankStatementLines',
    'SearchBankUnreconciledsLinesStart', 'CreateBankStatement',
    'SearchBankUnreconciledLines', 'CreateBankStatementStart']

_STATES = {'readonly': Eval('state') != 'draft'}
_DEPENDS = ['state']
CONFIRMED_STATES = {
    'readonly': Not(Equal(Eval('state'), 'draft'))
    }
CONFIRMED_DEPENDS = ['state']

_ZERO = Decimal("0.0")


class BankStatement(Workflow, ModelSQL, ModelView):
    'Bank Statement'
    __name__ = 'account.bank_statement'
    _rec_name = 'date'
    company = fields.Many2One('company.company', 'Company', required=True,
        states=_STATES, depends=['state'], select=True, domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', 0)),
            ])
    date = fields.Date('Date', required=True, states=_STATES,
        depends=['state'], help='Created date bank statement')
    bank_account = fields.Many2One('bank.account', 'Bank Account',
            required=True, states=_STATES)
    account = fields.Many2One('account.account', 'Account',
            required=True)
    bank = fields.Function(fields.Many2One('bank', 'Bank'),
            'on_change_with_bank')
    journal = fields.Many2One('account.journal', 'Journal',
            required=False, states=_STATES)
    period = fields.Many2One('account.period', 'Period', domain=[
            ('state', '=', 'open'),
            ('type', '=', 'standard'),
            ], states=_STATES)
    start_balance = fields.Numeric('Account Start Balance',
        required=True, digits=(16, 2), depends=['state'], states=_STATES)
    end_balance = fields.Function(fields.Numeric('Account End Balance',
        digits=(16, 2), states=_STATES,
        depends=['state']), 'get_end_balance')
    remainder = fields.Function(fields.Numeric('Remainder',
        digits=(16, 2), states=_STATES, depends=['state']),
        'get_remainder')
    total_remainder = fields.Function(fields.Numeric('Total Remainder',
        digits=(16, 2), states=_STATES, depends=['state', 'remainder']),
        'get_total_remainder')
    bank_start_balance = fields.Numeric('Bank Start Balance',
        required=True, digits=(16, 2), depends=['state'], states=_STATES)
    bank_end_balance = fields.Function(fields.Numeric('Bank End Balance',
        digits=(16, 2), depends=['lines']), 'get_bank_end_balance')
    lines = fields.One2Many('account.bank_statement.line', 'statement',
        'Lines', states=_STATES, context={
            'period': Eval('period'),
            'account': Eval('account'),
            'bank': Eval('bank'),
            }, depends=['period', 'account', 'bank'])
    state = fields.Selection([
            ('draft', 'Draft'),
            ('confirmed', 'Confirmed'),
            ('canceled', 'Canceled'),
            ], 'State', required=True, readonly=True)
    lines_no_transaction = fields.Function(fields.Many2Many('account.move.line',
        None, None, 'Moves Lines without Transaction', domain=[
            ('period', '=', Eval('period')),
            ('account', '=', Eval('account')),
        ]), 'get_lines_no_transaction')

    @classmethod
    def __setup__(cls):
        super(BankStatement, cls).__setup__()
        cls._order.insert(0, ('period', 'DESC'))
        cls._transitions |= set((
                ('draft', 'confirmed'),
                ('confirmed', 'draft'),
                ('canceled', 'draft'),
                ('draft', 'canceled'),
                ))
        cls._buttons.update({
                'confirm': {
                    'invisible': ~Eval('state').in_(['draft']),
                    'icon': 'tryton-forward',
                    },
                'draft': {
                    'invisible': ~Eval('state').in_(['canceled', 'confirmed']),
                    'icon': 'tryton-clear',
                    },
                'cancel': {
                    'invisible': Eval('state').in_(['canceled']),
                    'icon': 'tryton-cancel',
                    },
                })
        cls._error_messages.update({
                'lines_not_confirmed': ('There are lines on Statement not confirmed!'),
                'remainder_not_zero': ('The Total Remainder is not zero!'),
                'missing_config_account': ('Missing account definition on Bank Account!'),
                'cannot_delete_statement': ('Bank Statement "%s" cannot be deleted because '
                    'it contains lines.'),
                })

    @classmethod
    def validate(cls, statements):
        for statement in statements:
            if not statement.bank_account or not statement.bank_account.account:
                cls.raise_user_error('missing_config_account')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_date():
        return datetime.datetime.now()

    @staticmethod
    def default_start_balance():
        return Decimal('0.0')

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirm(cls, statements):
        for statement in statements:
            for line in statement.lines:
                if line.state != 'confirmed':
                    cls.raise_user_error('lines_not_confirmed')
            if statement.total_remainder != _ZERO:
                cls.raise_user_error('remainder_not_zero')

    @classmethod
    @ModelView.button
    @Workflow.transition('canceled')
    def cancel(cls, statements):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, statements):
        pass

    @classmethod
    def delete(cls, statements):
        for statement in statements:
            if statement.lines:
                cls.raise_user_error('cannot_delete_statement', statement.rec_name)
        super(BankStatement, cls).delete(statements)

    @fields.depends('bank_account')
    def on_change_with_bank(self, name=None):
        if self.bank_account:
            return self.bank_account.bank.id

    @fields.depends('bank_account')
    def on_change_bank_account(self, name=None):
        if self.bank_account and self.bank_account.account:
            self.account = self.bank_account.account.id

    def get_bank_end_balance(self, name):
        res = sum([l.moves_amount for l in self.lines])
        if self.bank_start_balance:
            res += self.bank_start_balance
        return res

    def get_remainder(self, name):
        return (self.bank_end_balance - self.end_balance)

    def get_total_remainder(self, name):
        val = sum([l.debit - l.credit for l in self.lines_no_transaction])
        return (val + self.remainder)

    def get_end_balance(self, name):
        balance = self.start_balance or Decimal(0)
        MoveLine = Pool().get('account.move.line')
        if self.period and self.bank_account and self.bank_account.account:
            move_lines = MoveLine.search([
                    ('period', '=', self.period.id),
                    ('account', '=', self.bank_account.account.id),
                ])
            period_balance = sum([l.debit - l.credit for l in move_lines])
        res = balance + period_balance
        return res

    def _search_update_lines(self):
        StatementLine = Pool().get('account.bank_statement.line')
        move_lines = self._search_related_lines()

        #Search current move lines inside statement lines
        current_move_lines = []
        for st_line in self.lines:
            current_move_lines.extend([l.id for l in st_line.bank_move_lines])

        st_lines_to_create = []
        for ml in move_lines:
            party_name = None
            description = None
            if ml.party and ml.party.name:
                party_name = ml.party.name
            if ml.id not in current_move_lines:
                if ml.description:
                    description = ml.description
                if party_name:
                    party_text = '[' + party_name + ']'
                    if description:
                        description = description + ' ' + party_text
                    else:
                        description = party_text
                if not description:
                    description = ml.account.name
                st_lines_to_create.append({
                    'statement': self.id,
                    'description': description,
                    'date': ml.move.date,
                    'state': 'draft',
                    'bank_move_lines': [('add', [ml.id])],
                })
        StatementLine.create(st_lines_to_create)

    def get_lines_no_transaction(self, name):
        move_lines = self._search_related_lines()
        targets = []

        for line in move_lines:
            if not line.bank_lines:
                targets.append(line.id)
            else:
                for bl in line.bank_lines:
                    if bl.bank_statement_line.statement.period != self.period:
                        targets.append(line.id)
        return targets

    def _search_related_lines(self):
        res = []
        if self.period and self.bank_account and self.bank_account.account:
                MoveLine = Pool().get('account.move.line')
                res = MoveLine.search([
                    ('period', '=', self.period.id),
                    ('account', '=', self.bank_account.account.id),
                ])
        return res


class BankStatementLine(Workflow, ModelSQL, ModelView):
    'Bank Statement Line'
    __name__ = 'account.bank_statement.line'
    _rec_name = 'description'
    statement = fields.Many2One('account.bank_statement', 'Bank Statement',
        required=True)
    date = fields.Date('Date', required=True, states=CONFIRMED_STATES)
    sequence = fields.Integer('Sequence', states=CONFIRMED_STATES)
    description = fields.Char('Description', required=True,
        states=CONFIRMED_STATES)
    bank_description = fields.Char('Bank Description', states=CONFIRMED_STATES)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('confirmed', 'Confirmed'),
            ], 'State', readonly=True)
    bank_move_lines = fields.Many2Many('bank_statement.line-account.move.line',
        'bank_statement_line', 'move_line', 'Bank Move Line',
            states=CONFIRMED_STATES, depends=['state', 'statement'],
                domain=[
                    ('account', '=', Eval('_parent_statement', {}).get('account')),
                ])
    moves_amount = fields.Function(fields.Numeric('Moves Amount',
            digits=(16, 2), depends=['amount']), 'get_moves_amount')
    transactions = fields.One2Many('account.bank_statement.transaction',
            'line', 'Transactions', states=CONFIRMED_STATES,
        context={
                'date': Eval('date'),
                'description': Eval('description'),
            }, depends=['statement'])

    @classmethod
    def __setup__(cls):
        super(BankStatementLine, cls).__setup__()
        cls._order.insert(0, ('date', 'ASC'))
        cls._transitions |= set((
                ('draft', 'confirmed'),
                ('confirmed', 'draft'),
                ))
        cls._buttons.update({
                'confirm': {
                    'invisible': Eval('state') == 'confirmed',
                    },
                'draft': {
                    'invisible': Eval('state') == 'draft',
                    },
                'search_reconcile': {
                    'invisible': Eval('state') == 'confirmed',
                    },
                })
        cls._error_messages.update({
                'not_bank_move_lines': ('There is not Bank Move Lines in '
                    'statement line!'),
                })

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_date():
        res = None
        period_id = Transaction().context.get('period')
        Period = Pool().get('account.period')
        if period_id:
            period = Period(period_id)
            res = period.start_date
        return res

    def get_moves_amount(self, name):
        res = sum([(ml.debit - ml.credit) for ml in self.bank_move_lines])
        return res

    @classmethod
    @ModelView.button_action('account_bank_statement.wizard_search_lines_unreconciled')
    def search_reconcile(cls, st_lines):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirm(cls, lines):
        for line in lines:
            for t in line.transactions:
                move = t.create_move()
                if move:
                    t.write([t], {'move': move})
            if not line.bank_move_lines:
                cls.raise_user_error('not_bank_move_lines')

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, lines):
        pass

    @classmethod
    def search(cls, args, offset=0, limit=None, order=None, count=False,
            query=False):
        """
        Override default search function so that if the user sorts by one field
        only, then 'sequence' is always added as a second sort field. This is
        specially important when the user sorts by date (the most usual) to
        ensure all moves from the same date are properly sorted.
        """
        if order is None:
            order = []
        order = list(order)
        if len(order) == 1:
            order.append(('sequence', order[0][1]))
        return super(BankStatementLine, cls).search(args, offset, limit, order,
            count, query)


class BankReconciliation(ModelSQL):
    'Bank Reconciliation'
    __name__ = 'bank_statement.line-account.move.line'
    _table = 'bank_statement_line_account_move_line'
    bank_statement_line = fields.Many2One('account.bank_statement.line',
        'Bank Statement Line', ondelete='CASCADE', select=True,
        required=True)
    move_line = fields.Many2One('account.move.line', 'Move Line',
        select=True, required=True, ondelete='RESTRICT')


class BankStatementTransaction(ModelSQL, ModelView):
    'Bank Statement Transaction'
    __name__ = 'account.bank_statement.transaction'
    line = fields.Many2One('account.bank_statement.line', 'Line',
        required=True, ondelete='CASCADE')
    date = fields.Date('Date', required=True)
    amount = fields.Numeric('Amount', required=True, digits=(16, 2))
    party = fields.Many2One('party.party', 'Party')
    account = fields.Many2One('account.account', 'Account', required=True,
        domain=[('kind', '!=', 'view')], depends=['line'])
    description = fields.Char('Description')
    move = fields.Many2One('account.move', 'Account Move', readonly=True)

    @classmethod
    def __setup__(cls):
        super(BankStatementTransaction, cls).__setup__()
        """
        # FIXME: Use python-sql
        cls._sql_constraints += [(
                'check_bank_move_amount', 'CHECK(amount != 0)',
                'Amount should be a positive or negative value.'),
        ]
        """
        cls._error_messages.update({
                'debit_credit_account_not_bank_reconcile': (
                    'The credit or debit account of Journal "%s" is not '
                    'checked as "Bank Conciliation".'),
                'account_not_reconcilable': ('The account "%s" is not reconcilable, '
                 'please check as true the reconcilable field.'),
                'missing_account_journal': ('Missing account '
                    'credit or debit on journal "%s".'),
                'same_debit_credit_account': ('Account "%(account)s" in '
                    'statement line "%(line)s" is the same as the one '
                    'configured as credit or debit on journal.'),
                })

    @staticmethod
    def default_party():
        context = Transaction().context
        Bank = Pool().get('bank')
        if 'bank' in context:
            return Bank(context['bank']).party.id

    @staticmethod
    def default_account():
        context = Transaction().context
        Bank = Pool().get('bank')
        if 'bank' in context:
            bank = Bank(context['bank'])
            if bank.account_expense:
                return bank.account_expense.id

    @staticmethod
    def default_description():
        context = Transaction().context
        if 'description' in context:
            return Transaction().context.get('description')

    @staticmethod
    def default_date():
        if Transaction().context.get('date'):
            return Transaction().context.get('date')
        return None

    @fields.depends('account', 'amount', 'party')
    def on_change_party(self):
        if self.party and self.amount:
            if self.amount > Decimal("0.0"):
                account = self.account or self.party.account_receivable
            else:
                account = self.account or self.party.account_payable
            self.account = account.id

    @fields.depends('amount', 'party', 'account')
    def on_change_amount(self):
        if self.party and not self.account and self.amount:
            if self.amount > Decimal("0.0"):
                account = self.party.account_receivable
            else:
                account = self.party.account_payable
            self.account = account.id

    def get_rec_name(self, name):
        return self.line.rec_name

    @classmethod
    def copy(cls, lines, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default.setdefault('move', None)
        return super(BankStatementTransaction, cls).copy(lines, default=default)

    def create_move(self):
        '''
        Create move for the statement line and return move if created.
        '''
        pool = Pool()
        Move = pool.get('account.move')
        BankReconciliation = pool.get('bank_statement.line-account.move.line')

        if self.move:
            return

        lines_to_create = self._create_move_lines()
        values = {
            'company': self.line.statement.company.id,
            'period': self.line.statement.period.id,
            'journal': self.line.statement.journal.id,
            'date': self.date,
            'lines':[('create', lines_to_create)],
            'description': self.description,
        }
        move, = Move.create([values])

        lines_to_reconcile = [ml.id for ml in move.lines if ml.account.id == self.line.statement.account.id]
        for ln in lines_to_reconcile:
            BankReconciliation.create([{
                    'move_line': ln,
                    'bank_statement_line': self.line.id,
            }])
        move.save()
        return move

    @classmethod
    def post_move(cls, lines):
        Move = Pool().get('account.move')
        Move.post([l.move for l in lines if l.move])

    @classmethod
    def delete_move(cls, lines):
        Move = Pool().get('account.move')
        Move.delete([l.move for l in lines if l.move])

    def _create_move_lines(self):
        '''
        Return the move lines for the statement line
        '''
        amount = self.amount

        if not self.line.statement.account.bank_reconcile:
            self.raise_user_error('account_not_reconcilable',
                self.line.statement.account.rec_name)
        if self.account.id == self.line.statement.account.id:
            self.raise_user_error('same_debit_credit_account', {
                    'account': self.line.statement.account.rec_name,
                    'line': self.account.rec_name,
                    })

        product_line = {
                'description': self.description,
                'debit': amount < _ZERO and -amount or _ZERO,
                'credit': amount >= _ZERO and amount or _ZERO,
                'account': self.account.id,
        }
        if self.account.party_required:
            product_line['party'] = self.party.id

        bank_line = {
            'description': self.description,
            'debit': amount >= _ZERO and amount or _ZERO,
            'credit': amount < _ZERO and -amount or _ZERO,
            'account': self.line.statement.account.id,
        }

        if self.line.statement.account.party_required:
            product_line['party'] = self.party.id
        return [bank_line, product_line]


class UpdateBankStatementLines(Wizard):
    'Update Bank Statement Lines'
    __name__ = 'account.bank_statement.update_lines'
    start_state = 'update_lines'
    update_lines = StateTransition()

    def transition_update_lines(self):
        BankStatement = Pool().get('account.bank_statement')
        ids = Transaction().context['active_ids']
        if ids:
            id_ = ids[0]
            statement = BankStatement(id_)
            if statement.state == 'draft':
                statement._search_update_lines()
        return 'end'


class SearchBankUnreconciledsLinesStart(ModelView):
    'Search Bank Unreconciled Lines (Start)'
    __name__ = 'account_bank_statement.search_bank_unreconciled_lines.start'
    move_lines = fields.Many2Many('account.move.line', None, None, 'Move Line', required=True,
        domain=[
            ('account', '=', Eval('account')),
            ('bank_lines', '=', None),
            ])
    account = fields.Many2One('account.account', 'Account',
            domain=[('kind', '!=', 'view')], select=True)

    @staticmethod
    def default_account():
        pool = Pool()
        StLine = pool.get('account.bank_statement.line')
        st_line_id = Transaction().context.get('active_id')
        st_line = StLine(st_line_id)
        return st_line.statement.account.id


class SearchBankUnreconciledLines(Wizard):
    'Search Bank Unreconciled Lines'
    __name__ = 'account_bank_statement.search_bank_unreconciled_lines'

    start = StateView('account_bank_statement.search_bank_unreconciled_lines.start',
        'account_bank_statement.search_bank_lines_unreconciled_view_form', [
                Button('Cancel', 'end', 'tryton-cancel'),
                Button('Add', 'search_', 'tryton-ok', default=True),
            ])
    search_ = StateTransition()

    def transition_search_(self):
        pool = Pool()
        Line = pool.get('account.bank_statement.line')
        line = Line(Transaction().context.get('active_id'))
        for move_line in self.start.move_lines:
            BankReconciliation.create([{
                    'move_line': move_line,
                    'bank_statement_line': line.id,
                    }])
        return 'end'


class CreateBankStatementStart(ModelView):
    'Create Bank Statement Start'
    __name__ = 'account_bank_statement.create_bank_statement.start'
    bank_account = fields.Many2One('bank.account', 'Bank Account')


class CreateBankStatement(Wizard):
    'Create Bank Statement'
    __name__ = 'account_bank_statement.create_bank_statement'
    start = StateView('account_bank_statement.create_bank_statement.start',
        'account_bank_statement.create_bank_statement_view_form', [
                Button('Cancel', 'end', 'tryton-cancel'),
                Button('Add', 'create_', 'tryton-ok', default=True),
            ])
    create_ = StateTransition()

    def transition_create_(self):
        pool = Pool()
        BankStatement = pool.get('account.bank_statement')
        Configuration = pool.get('account.bank_statement.configuration')

        values = {
                'bank_account': self.start.bank_account.id,
                'date': datetime.datetime.now(),
                'start_balance': _ZERO,
                'bank_start_balance': _ZERO,
                }

        if Configuration(1).default_journal_bank:
            journal_id = Configuration(1).default_journal_bank.id
            values['journal'] = journal_id

        Period = pool.get('account.period')

        current_bank_statements = BankStatement.search([
                ('bank_account', '=', self.start.bank_account.id),
                ])
        periods_done = []
        bs_dates = []
        for bs in current_bank_statements:
            periods_done.append(bs.period.id)
            bs_dates.append(bs.period.start_date)
        bs_max_date = max(bs_dates)
        period_pending = Period.search([
                    ('type', '=', 'standard'),
                    ('id', 'not in', periods_done),
                    ('start_date', '>', str(bs_max_date)),
                ], order=[('start_date', 'ASC')], limit=1)
        if period_pending:
            values['period'] = period_pending[0].id

        bank_statements = BankStatement.create([values])
        bank_statements[0]._search_update_lines()
        return 'end'
