# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['Bank', 'BankAccount']


class Bank(metaclass=PoolMeta):
    __name__ = 'bank'
    account_expense = fields.Many2One('account.account', 'Account')


class BankAccount(metaclass=PoolMeta):
    __name__ = 'bank.account'
    account = fields.Many2One('account.account', 'Account',
        required=False, domain=[
            ('kind', '!=', 'view'),
            ('bank_reconcile', '=', True),
        ])
